<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'database' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'PO;{0:7C8fZ$rYTgf/9(J3?Bi`EI{9aEFUdaD+oYXXkS:+Ra@t%kksXVuCPoF<<_' );
define( 'SECURE_AUTH_KEY',  'F[y4z~dKD85r@fK%Bw`h6+k<jHZqau_+(Ps%1MRY#-+X{}oaDApmi-vue!1R6[m2' );
define( 'LOGGED_IN_KEY',    '?BHWTt;Qnjd@lI>-D;}bS3.(B(9f>v~M~z`}%3)yNiq=P,[YoiUnPRtTpn>y!,KC' );
define( 'NONCE_KEY',        'U{n`1&Q 5ZwfjT-y=Q0a:BvLiq`4{TG(oR-NfxFw/kTbBKDrN-6i$sv}_q|e!yJ5' );
define( 'AUTH_SALT',        'Dwm+yr|dQ`?7^1W}#->@VXY5{u5*9YL0kRZ~U3o8t5N[R]Q-AMc+M`uH%!CvE(Cm' );
define( 'SECURE_AUTH_SALT', '[r;<c=Cmt?TGbT`(2KW,j#N[jRTY{:VE<ILc]v/]sy-?`uhme)m7^aVBdd 8.gxJ' );
define( 'LOGGED_IN_SALT',   'p7F{(rXlu%Sh7rc{~K;Q|!BlFC)G$O!(gD $7[sou=|i3fWS*j[/TVT*7*srcMrp' );
define( 'NONCE_SALT',       'A.>ypne+a=^3F]wd,1+G$kLDYQWh_K:U-{WG>+B]w<ZA@Dt&[r` CaDQoYCdSTzP' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
