<?php get_header(); ?>

<main class="container">

	<div class="row">
		<article class="post__article col-12 col-md-8">
			<h1 class="post__title">
				<?php the_title() ?>
			</h1>

			<section class="post__content">
				<?php the_content() ?>
			</section>

			<nav class="post__navigation">
				<?php make_post_navigation() ?>
			</nav>
		</article>

		<aside class="post__sidebar col-12 col-md-4">
			<?php make_post_sidebar() ?>
		</aside>
	</div>

</main>

<?php get_footer(); ?>