<?php get_header(); ?>

<main class="container">

	<div class="row">
		<article class="post__article col-8">
			<h1 class="post__title">
				<?php the_title() ?>
			</h1>

			<section class="post__content">
				<?php the_content() ?>
			</section>

			<nav class="post__navigation">
				<?php make_post_navigation() ?>
			</nav>
		</article>

	</div>

</main>

<?php get_footer(); ?>