<?php
if (is_author()) show_404();            # 404 for author archive page 
if (is_category()) show_404();          # 404 for category archive page 
if (is_tag()) show_404();               # 404 for tag archive page 
if (is_date()) show_404();              # 404 for archive pages by date 
if (is_tax('', 'country')) show_404();  # 404 for taxonomy with slug 'country'
?>

<?php get_header(); ?>

<main class="container">

    <?php if (have_posts()) : ?>

        <?php if (is_tax()) : ?>
            <?php $term = get_queried_object() ?>
            <?php $tax_name = get_taxonomy_labels(get_taxonomy($term->taxonomy))->singular_name ?>
            <?php $term_name = $term->name ?>
            <?php $term_desc = esc_html($term->description) ?>
            <h1>
                This is Category
                "<?php echo $term_name ?>" from
                "<?php echo $tax_name ?>" Taxonomy
            </h1>
            <p><?php echo $term_desc ?></p>
        <?php endif ?>

        <?php while (have_posts()) : the_post(); ?>

            <?php get_template_part('template-parts/content'); ?>

        <?php endwhile; ?>

    <?php else : ?>

        <?php get_template_part('template-parts/content', 'none'); ?>

    <?php endif; ?>

</main>

<?php get_footer(); ?>