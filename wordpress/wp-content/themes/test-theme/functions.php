<?php

/**
 * Enqueue scripts & styles
 */
function cool_club_enqueue_scripts()
{
  $ver_bootstrap = '4.5.2';
  $ver_jquery = '3.5.1';
  $ver_ionicons = '4.5.10-0';

  #=== Styles

  # Bootstrap
  wp_enqueue_style(
    "bootstrap",
    "https://stackpath.bootstrapcdn.com/bootstrap/${ver_bootstrap}/css/bootstrap.min.css",
    array(),
    $ver_bootstrap
  );

  # Ionicons
  wp_enqueue_style(
    "ionicons",
    "https://unpkg.com/ionicons@${ver_ionicons}/dist/css/ionicons.min.css",
    array("bootstrap"),
    $ver_ionicons
  );

  # Theme style
  wp_enqueue_style(
    "cool-club-style",
    get_template_directory_uri() . '/style.css',
    array("bootstrap"),
    filemtime(get_theme_file_path('style.css'))
  );

  #=== Javascript

  # JQuery
  wp_deregister_script('jquery');
  wp_enqueue_script(
    "jquery",
    "https://code.jquery.com/jquery-${ver_jquery}.min.js",
    array(),
    $ver_jquery
  );

  # Bootstrap
  wp_enqueue_script(
    'bootstrap',
    "https://stackpath.bootstrapcdn.com/bootstrap/${ver_bootstrap}/js/bootstrap.min.js",
    array('jquery'),
    $ver_bootstrap,
    'in_footer'
  );
}

add_action('wp_enqueue_scripts', 'cool_club_enqueue_scripts');


/**
 * Custom post navigation
 */
function make_post_navigation()
{
  get_template_part('template-parts/posts', 'nav');
}

/**
 * Custom posts sidebar
 */
function make_post_sidebar($num_posts = 3)
{
  $current_post_id = get_the_ID();
  $posts = get_posts(array(
    'numberposts' => $num_posts,
    'exclude' => array($current_post_id)
  ));

  get_template_part('template-parts/posts', 'sidebar', array('posts' => $posts));
}

/**
 * Shortcode: download-file (should work only for pages)
 */
function shortcode_download_file($atts, $content, $tag)
{
  $atts = shortcode_atts(array(
    'title' => __('Download File', 'cool-club'),
    'url' => null
  ), $atts);

  # exit early if not on page
  if (!is_page()) return;

  # exit early if no url given
  if (is_null($atts['url']) || empty($atts['url'])) return;

  $title = esc_html($atts['title']);
  $url = $atts['url'];

  return "<a href='${url}' title='${title}'>${title}</a>";
}

add_shortcode('download-file', 'shortcode_download_file');

/**
 * Redirect to 404
 */
function show_404()
{
  global $wp_query;
  $wp_query->set_404();
  status_header(404);
  get_template_part(404);
  exit();
}
