<?php

#=== Available variables
#
# $args['posts'] - array of posts

?>

<ul class="nav flex-column">
  <?php foreach ($args['posts'] as $post) : ?>
    <?php setup_postdata($post) ?>

    <li class="nav-item">
      <a class="nav-link active" href="<?php the_permalink() ?>">
        <?php the_title() ?>
      </a>
    </li>

  <?php endforeach ?>
  <?php wp_reset_postdata() ?>
</ul>