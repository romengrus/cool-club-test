<ul class="nav justify-content-center">
  <li class="nav-item">
    <?php $prev_post = get_previous_post(true); ?>
    <?php if (!empty($prev_post)) : ?>

      <?php $href = get_permalink($prev_post) ?>
      <?php $title = esc_html($prev_post->post_title) ?>

      <a class="nav-link" href="<?php echo $href ?>">
        <i class="icon ion-md-arrow-back"></i>
        <?php echo $title ?>
      </a>

    <?php endif ?>
  </li>
  <li class="nav-item">
    <?php $next_post = get_next_post(true); ?>
    <?php if (!empty($next_post)) : ?>

      <?php $href = get_permalink($next_post) ?>
      <?php $title = esc_html($next_post->post_title) ?>

      <a class="nav-link" href="<?php echo $href ?>">
        <?php echo $title ?>
        <i class="icon ion-md-arrow-forward"></i>
      </a>

    <?php endif ?>
  </li>
</ul>